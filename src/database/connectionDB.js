const mongoose = require('mongoose');
const { db } = require('./urlDB');

class ConnDb {
    constructor() {
        this.connection();
    }

    async connection() {
        try {
            this.conn = await mongoose.connect(db);
            console.log("conexión exitosa a la base de datos")
        } catch (error) {
            console.log(error)
        }
    }
}

module.exports = ConnDb;