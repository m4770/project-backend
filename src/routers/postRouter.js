const { Router } = require('express');

//Módulos
const TokenController = require('../controllers/tokenController');
const PostController = require('../controllers/postController');

class PostRouter {
    constructor() {
        // Crear ruta como atributo de la clase
        this.router = Router();
        // Se llama al método para crear las rutas
        this.config();
    }

    config() {
        // Instancio la clase de Post Controller
        const postController = new PostController();

        // <------- Rutas pùblicas ----->
        // Obtener todos los post
        this.router.get('/posts', postController.getAll);

        const tokenController = new TokenController();
        // Middleware
        this.router.use(tokenController.verifyAuth);

        // <----- Rutas privadas ---->
        // Crear un post
        this.router.post('/post', postController.create);
        //Obtener post por usuario
        this.router.get('/post', postController.getByUser);
        //Actualizar post por usuario
        this.router.put('/post',postController.update);
        //Eliminar post por usuario
        this.router.delete('/post',postController.delete);
    }
}

module.exports = PostRouter;