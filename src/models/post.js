const { Schema, model } = require('mongoose');

const postSchema = Schema({
    title: { type: String },
    category: { type: String },
    quantity: { type: Number },
    unity: { type: String },
    url_img: { type: String },
    user_id: { type: String }
}, {
    collection: 'posts'
});

module.exports = model('Post', postSchema);