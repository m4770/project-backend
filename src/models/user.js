const { Schema, model } = require('mongoose');

const userSchema = Schema({
    name: { type: String },
    email: { type: String },
    password: { type: String },
    address: { type: String },
    phone: { type: String }
}, {
    collection: 'users'
});

module.exports = model('User', userSchema);