const jwt = require('jsonwebtoken');

class TokenController {

    //Función para obtener el token
    getToken = (req) => {
        let token = null;
        // Capturar el beater token de la cabecera
        let authorization = req.headers.authorization;

        if (authorization !== null && authorization !== undefined) {
            // Tomar el token de la cadena de texto
            token = authorization.split(" ")[1];
        }
        return token;
    }

    //Función para obtener el id del usuario
    getUserId = (req) =>{
        // Obtener el token de la peticion
        let token = this.getToken(req);
        // Decodificar el token para obtener el id del usuario
        let decode = jwt.decode(token, process.env.NODE_PRIVATE_KEY);
        //Obtener el user_id
        let user_id = decode.id;

        return user_id;
    }

    //Función para verificar token del usuario
    verifyAuth = (req, res, next) => {
        // Obtener el token
        let token = this.getToken(req);
        //Verificar token
        jwt.verify(token, process.env.NODE_PRIVATE_KEY, (error, decode) => {
            if (error) {
                res.status(401).json({ message: "usuario no autenticado" })
            } else {
                next();
            }
        });
    }
}

module.exports = TokenController;