const User = require('../models/user');
const jwt = require('jsonwebtoken');

class UserController {

    //Función para crear un usuario
    register(req, res) {
        let newUser = req.body;
        let { name, email, password, address, phone } = newUser;

        if (name && email && password && address && phone) {
            User.create(newUser, (error, doc) => {
                if (error) {
                    res.status(500).json({ message: "Error insercción de usuario" });
                } else {
                    let token = jwt.sign({ id: doc._id }, process.env.NODE_PRIVATE_KEY);
                    res.status(201).json({ token });
                }
            });
        } else {
            res.status(400).json({ message: "datos incompletos" });
        }
    }

    //Función para autenticar usuario
    login(req, res) {
        let { email, password } = req.body;
        User.find({ email, password }, (error, docs) => {
            if (error) {
                res.status(500).send();
            } else {
                if (docs.length > 0) {
                    let user = docs[0];
                    let token = jwt.sign({ id: user._id }, process.env.NODE_PRIVATE_KEY);
                    res.status(200).json({ token, name: user.name });
                } else {
                    res.status(401).json({ message: "credenciales incorrectas" });
                }
            }
        })
    }
}

module.exports = UserController;