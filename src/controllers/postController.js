
// Módulos
const Post = require('../models/post');
const TokenController = require('./tokenController');

class PostController {

    constructor() {
        // Crear atributo de la clase tipo Token Controller
        this.tokenController = new TokenController();
    }

    //Función para crear un post
    create = (req, res) => {
        // obtener datos del cuerpo de la petición
        let { title, category, quantity, unity, url_img } = req.body;
        // obtener el user id
        let user_id = this.tokenController.getUserId(req);

        Post.create({ title, category, quantity, unity, url_img, user_id }, (error, doc) => {
            if (error) {
                res.status(500).json({ error });
            } else {
                res.status(201).json({ message: "post creado con éxito", post: doc })
            }
        })
    }

    //Función para obtener los post creado por un usuario
    getByUser = (req, res) => {

        let user_id = this.tokenController.getUserId(req);

        Post.find({ user_id }, (error, posts) => {
            if (error) {
                res.status(500).json({ error });
            } else {
                res.status(200).json({ posts })
            }
        })
    }

    //Función para actualizar un post
    update = (req, res) => {
        // Obtener datos del post a editar
        let { id, title, category, quantity, unity, url_img } = req.body;

        // Obtener user id
        let user_id = this.tokenController.getUserId(req);

        Post.findOneAndUpdate({ _id: id, user_id }, { title, category, quantity, unity, url_img }, (error, post) => {
            if (error) {
                res.status(500).json({ error });
            } else {
                res.status(200).json({ message: "post actualizado" });
            }
        })
    }

    //Función para eliminar un post
    delete = (req, res) => {
        //Obtener id del post del cuerpo de la petición
        let { id } = req.body;
        //Obtener user id
        let user_id = this.tokenController.getUserId(req);

        Post.findOneAndRemove({ _id: id, user_id }, (error, doc) => {
            if (error) {
                res.status(500).json({ error });
            } else {
                if (doc) {
                    res.status(200).json({ message: 'Post eliminado con éxito' });
                } else {
                    res.status(200).json({ message: "No se encontró ningún post" });
                }
            }
        })
    }

    //Función para obtener todos los post
    getAll = (req, res) => {
        Post.find((error, posts) => {
            if (error) {
                res.status(500).json({ error });
            } else {
                res.status(200).json({ posts })
            }
        })
    }
}

module.exports = PostController;